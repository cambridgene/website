<!--div>
<p class="text-center">We will be relaunching our website soon. Please get in touch for further information:  <strong>info@cambridgene.com</strong></p>
</div-->
<hr/>


<!--div id="footer-panel">
    <div class="container">
        <div class="row light">
            <div class="col-md-2">
                <ul>
                    <li>
                        <a href="index.php">Home</a>
                    </li>
                    <li>
                        <a href="index.php#solutions">Solutions</a>
                    </li>
                    <li>
                        <a href="index.php#expertise">Expertise</a>
                    </li>
                    <li>
                        <a href="Overview.php">About</a>
                    </li>
                    <li>
                        <a href="Contact.php">Contact</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
             Contact Us: <strong>info@cambridgene.com</strong>
             <p>Future Business Centre,<br>Kings Hedges Road, Cambridge CB4 2HY</p>
                <p>Stevenage Bioscience Catalyst,<br> Gunnels Wood Road, Stevenage SG1 2FX</p>
                <p>Contact Us:&#032;&#105;&#110;&#102;&#111;&#064;&#099;&#097;&#109;&#098;&#114;&#105;&#100;&#103;&#101;&#110;&#101;&#046;&#099;&#111;&#109;</p>

            </div>
            <div class="col-md-6">
                <div class="rpm" >
                    &nbsp;
                </div>
            </div>
        </div>
    </div>
</div-->
    <footer>
        <div class="container text-center">
            <div class="row">
                <div class="col-md-12 text-center">
                    <ul class="list-inline quicklinks">
                        <li><span class="copyright"> &copy; 2018. Cambridgene. All rights reserved.</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
