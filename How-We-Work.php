<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cambridgene</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php 
            include_once("page-includes.php");
        ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <?php 
            include_once("header.php");
        ?>
        <!--
        <div class="cb-slideshow">
            <header id="landing-page">
                <div class="container">
                    <div class="page-title shadow">
                        <div class="">Welcome To Cambridgene!</div>
                    </div>
                </div>
            </header>
        </div>
    -->
        <section class="bb nbb c-section">
            <div class="container">
                <div class="col-md-8 col-md-offset-2 para-norm">
                    <h2 class="section-sub-heading nbb">How We Work</h2>
                
<h2 class="section-sub-heading-2 nbb">Planning and Assessment</h2>
<p>For each potential project, we review the requirements in terms of resources, timelines (for development, delivery/deployment, training/support), additional consulting and external resources. We will then provide a customized and competitive quote, taking into account the size and corporate nature of the customer.
</p>
<p>For transferable applications and databases, we provide bundled consulting for customization, deployment and delivery. In addition, all deliverables include ample on-site/off-site training and support provisions to accelerate the usage of our products.
</p>

<p>We also provide the opportunity of collaborative product development in the aim of providing early availability of features/modules with an advantageous cost structure.
</p>

<h2 class="section-sub-heading-2 nbb">Qualification and Feasibility</h2>
<p>After the initial assessment and reception of data, we make sure that the data passes the minimum qualification criteria for the planned analysis. This will ensure the feasibility of the originally planned analysis or any required modifications in the plan for further analysis. 
</p>
<p>The customer receives a brief report outlining the above. Upon approval, we proceed with further analysis.
</p>
<h2 class="section-sub-heading-2 nbb">Reproducibility and Documentation</h2>
<p>
Our analysis is documented and communicated clearly to the customer. All steps in the analysis are described including (but not limited to) data, software, versions, steps, etc.
</p>
<p>
We make sure that the work is fully reproducible at any time later, as we preserve all the settings for every analysis. This can be reviewed or rerun later for the confirmation of results.
</p>
<h2 class="section-sub-heading-2 nbb">Validation</h2>

<p>In all of our projects, we validate the analysis and results thoroughly. We also expect our customers to independently evaluate the results to ensure the scientific validity. 
</p>

            </div>
        </section>
        

        <?php 
            include_once("footer.php");
        ?>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="assets/js/vendor/bootstrap.js"></script>
        <script src="assets/js/vendor/jquery.validate.min.js"></script>
        <script src="assets/js/vendor/additional-methods.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>

        
    </body>
</html>
