<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cambridgene</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php 
            include_once("page-includes.php");
        ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <?php 
            include_once("header.php");
        ?>
        
        <div class="cb-slideshow">
            <header id="landing-page">
                <div class="container">
                    <div class="page-title shadow">
                        <div class="">Welcome To Cambridgene!</div>
                    </div>
                </div>
            </header>
        </div>
        <section class="nbp">
            <div class="container">
                <div class="heading text-center">
                    Cambridgene unlocks the power of genomics to accelerate and de-risk pharmaceutical research and development. Our advanced clinical research workflows and genetic profiling solutions accelerate the development and delivery of precision medicines.
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <div class="row">
                    <h2 class="text-center section-heading">Our Solutions</h2>
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="col-sm-4">
                            <div class="box">
                                <p class="text-center"><i class="fa fa-sort-amount-asc fa-4x"></i></p>
                                <h4 id="download-bootstrap">Bio Marker Stratification</h4>
                                <p>Compiled and minified CSS, JavaScript, and fonts. No docs or original source files are included.</p>
                                
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="box">
                                <p class="text-center"><i class="fa fa-rocket fa-4x"></i></p>
                                <h4 id="download-bootstrap">Accelerate Antibody R&amp;D</h4>
                                <p>Compiled and minified CSS, JavaScript, and fonts. No docs or original source files are included.</p>
                                
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="box">
                                <p class="text-center"><i class="fa fa-bar-chart fa-4x"></i></p>
                                <h4 id="download-bootstrap">Health Demographics</h4>
                                <p>Compiled and minified CSS, JavaScript, and fonts. No docs or original source files are included.</p>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>  
         <div class="cb-slideshow">
            <header class="dark">
                <section>
                <div class="container">
                    <h2 class="section-heading">Our Expertise</h2>
                    <div class="text-left col-sm-10 col-sm-offset-1">
                        <div class="col-sm-6">
                            <div class="text-right">
                                <h4 id="download-bootstrap">Data Integration and Quality</h4>
                                <p>Compiled and minified CSS, JavaScript, and fonts. No docs or original source files are included.</p>
                                
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="">
                                <h4 id="download-bootstrap">Genomic Variation Analysis</h4>
                                <p>Compiled and minified CSS, JavaScript, and fonts. No docs or original source files are included.</p>
                                
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-6">
                            <div class="text-right">
                                <h4 id="download-bootstrap">Bio Marker Analysis</h4>
                                <p>Compiled and minified CSS, JavaScript, and fonts. No docs or original source files are included.</p>
                                
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="">
                                <h4 id="download-bootstrap">Public Domain Data Access</h4>
                                <p>Compiled and minified CSS, JavaScript, and fonts. No docs or original source files are included.</p>
                                
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="text-center">
                                <h4 id="download-bootstrap">Advanced Workflows</h4>
                                <p>Compiled and minified CSS, JavaScript, and fonts. No docs or original source files are included.</p>
                                
                            </div>
                        </div>
                    </div>
                </div>
               </section>
            </header>
        </div>

        <?php 
            include_once("footer.php");
        ?>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="assets/js/vendor/bootstrap.js"></script>
        <script src="assets/js/vendor/jquery.validate.min.js"></script>
        <script src="assets/js/vendor/additional-methods.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>

        
    </body>
</html>
