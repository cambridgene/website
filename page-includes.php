		<?php 
		    include_once("fav-includes.php");
		?>

        <!-- Bootstrap and fontawesome CSS -->
        <link rel="stylesheet" href="assets/css/bootstrap-theme.css">
        <link rel="stylesheet" href="assets/css/bootstrap.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/font-awesome.css">
        <link rel="stylesheet" href="assets/css/normalize.css">

        <link rel="stylesheet" href="assets/css/main.css">

        <!-- Custom Fonts -->
        <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <!-- font-family: Montserrat,"Helvetica Neue",Helvetica,Arial,sans-serif; -->
        <link href='http://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
        <!-- -->
        <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <!-- font-family: "Droid Serif","Helvetica Neue",Helvetica,Arial,sans-serif; -->
        <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
        <!--  font-family: "Roboto Slab","Helvetica Neue",Helvetica,Arial,sans-serif; -->
        <link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
        <!-- font-family: 'Great Vibes', cursive; -->
        <link href='http://fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
        <!-- font-family: "Poiret One",cursive; -->
        <link href='http://fonts.googleapis.com/css?family=Waiting+for+the+Sunrise' rel='stylesheet' type='text/css'>

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <!-- font-family: 'Open Sans', sans-serif; -->




        <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>