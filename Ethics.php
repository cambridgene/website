<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cambridgene</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php 
            include_once("page-includes.php");
        ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <?php 
            include_once("header.php");
        ?>
        <!--
        <div class="cb-slideshow">
            <header id="landing-page">
                <div class="container">
                    <div class="page-title shadow">
                        <div class="">Welcome To Cambridgene!</div>
                    </div>
                </div>
            </header>
        </div>
    -->
        <section class="bb nbb c-section">
            <div class="container">
                <div class="col-md-8 col-md-offset-2 para-norm">
                    <h2 class="section-sub-heading nbb">Ethics</h2>
<p>We at Cambridgene believe that our work should have clear ethics and research guidelines while delivering significant value to improve people’s lives.</p>
<p>Our practices strongly reflect the following ethics:</p>
<ul>
<li>The goal of clinical research should be clear to improve people’s lives and the understanding of disease biology and its solutions, through generalizable knowledge</li>
<li>The research questions that we need answered should have significant social and clinical value </li>
<li>We design studies or analyze data with a clear objective of answering a research question. We take steps to understand whether a research question is answerable with scientific validity</li>
<li>The primary objective for the data analysis, study design and reporting are carried out without any discrimination, with samples chosen to answer the questions, are selected and analyzed fairly</li>
<li>We expect our customers to independently review and validate our results in order to make it clear they are valid for any purpose including the objectives of the study</li>
<li>We respect the privacy and confidentiality of data, taking all possible steps to protect the information handled by us</li>
<li>We seek advice on a routine and contextual basis to understand the implications of our work</li>
</ul>
                </div>
            </div>
        </section>
        

        <?php 
            include_once("footer.php");
        ?>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="assets/js/vendor/bootstrap.js"></script>
        <script src="assets/js/vendor/jquery.validate.min.js"></script>
        <script src="assets/js/vendor/additional-methods.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>

       
    </body>
</html>
