<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cambridgene</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php 
            include_once("page-includes.php");
        ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <?php 
            include_once("header.php");
        ?>
        <!--
        <div class="cb-slideshow">
            <header id="landing-page">
                <div class="container">
                    <div class="page-title shadow">
                        <div class="">Welcome To Cambridgene!</div>
                    </div>
                </div>
            </header>
        </div>
    -->
        <section class="bb nbb c-section">
            <div class="container">
                <div class="col-md-8 col-md-offset-2 para-norm">
                    <h2 class="section-sub-heading nbb">Our Team</h2>

<p>
Cambridgene’s team includes the core team, business and scientific advisers and collaborators in academic research institutions.
</p>
<div>&nbsp;</div>
<div class="col-md-12">
    <div class="col-md-3"><div class="image-1">&nbsp;</div></div>
    <div class="col-md-9"> 
    <h2 class="section-sub-heading-2 nbb">Parthiban Vijayarangakannan – Co-founder and CEO</h2>
<p>
Parthiban was a postdoctoral fellow at the Wellcome Trust Sanger Institute and has a PhD from the International Max Planck Research School (Cologne, Germany). He contributed to the analysis/diagnosis of thousands of patients across multiple research projects and disease areas.</p>
    </div>
</div>
<div>&nbsp;</div>
<div class="col-md-12">
     <div class="col-md-9"> 
<h2 class="section-sub-heading-2 nbb">Balamurali Rathinam – Co-founder and CTO</h2>
    <p>
Balamurali has 15+ years of experience in product development for enterprise-level, cloud and big data services across multiple domains. He has offered cutting-edge SaaS solutions for large multinational organisations.
    </p>
    </div>
    <div class="col-md-3"> <div class="image-2">&nbsp;</div></div>
</div>
<div>&nbsp;</div>
<div class="col-md-12">
    <div class="col-md-3"> <img src="assets/img/tulasi.png" height="90%" width="90%"></div>
    <div class="col-md-9"> 
<h2 class="section-sub-heading-2 nbb">Tulasi Marrapu – Bioinformatician</h2>
        <p>
Tulasi Marrapu has over eight years of experience in programming and statistical data analysis in clinical and research domain. Tulasi worked several years as Chief Computing Officer in Department of Clinical Neurosciences, University of Cambridge, where she was involved in analysing MR Image and clinical data.
        </p>
    </div>
</div>
<div>&nbsp;</div>
<h2 class="section-sub-heading-2 nbb">Consultants</h2>

<p>

Cambridgene recruits or works in collaboration with external bioinformatics consultants for the design, development and delivery of products/solutions. For our core offerings (Solutions), most of the work is conducted in-house by our experienced personnel. 
</p>


<h2 class="section-sub-heading-2 nbb">Advisory Board</h2>

<h2 class="section-sub-heading-2 nbb">Business Advisers</h2>
<ul>
<li>Martino Picardo – Independent Consultant/ex-CEO, Stevenage Bioscience Catalyst</li>
<li>Paul Hughes – Director of Enterprise Support, Allia </li>
<li>Shailendra Vyakarnam – Director, Bettany Centre for Entrepreneurship / ex-Director, Centre for Entrepreneurial Learning, Judge Business School, Cambridge, UK</li>
<li>Belinda Bell – Programme Director, Cambridge Social Ventures </li>
<li>John Willis – CEO, Power2Inspire / ex-Chairman (Provide CIC) </li>
</ul>

<h2 class="section-sub-heading-2 nbb">Scientific Advisers</h2>

<ul>
<li>
Marc-Philipp Hitz, Pediatric Cardiologist / Scientist, Department of Congenital Heart Disease and Pediatric Cardiology, University of Kiel, Germany
http://www.kinderherzzentrum-kiel.de/
</li>

<li>
Chris-Tyler Smith, Group Leader, Wellcome Trust Sanger Institute, Cambridge, UK
https://www.sanger.ac.uk/research/faculty/ctylersmith/
</li>

<li>
Arthur Wuster, Computational Biologist, Genentech (formerly a postdoctoral fellow at the Sanger Institute), San Francisco, USA
https://www.linkedin.com/profile/view?id=126212901

<li>
Christoph Steinbeck, Professor for Analytical Chemistry, Friedrich-Schiller-University in Jena, Germany / ex-Team Leader of Chemoinformatics and Metabolism, EMBL – European Bioinformatics Institute, Cambridge, UK
http://www.ebi.ac.uk/about/people/christoph-steinbeck
</li>

</ul>
                </div>
            </div>
        </section>
        

        <?php 
            include_once("footer.php");
        ?>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="assets/js/vendor/bootstrap.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>

       
    </body>
</html>
